import blockSectionIntro from './blockSectionIntro';
import tabmodule from './tabModule';

const app = {

    options: {
        loadingIndicator: 'loadingIndicator'
    },

    init: function() {
        this.onLoad();

        blockSectionIntro.init();
        tabmodule.init();
    },

    onLoad: function() {

        window.onload = () => {
            [].forEach.call(document.querySelectorAll(`.${this.options.loadingIndicator}`), ( el ) => {
                el.classList.remove(`${this.options.loadingIndicator}`);
            });
        };
    }
};

app.init();
