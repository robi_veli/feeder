export default {

	defaults: {
		'behavior': 'smooth',
		'left': 0,
		'top': 0
	},

    init: function ( params ) {
        this.options = Object.assign({}, this.defaults, params);

		this.isSmoothScrollSupported = Boolean('scrollBehavior' in document.documentElement.style);
	},

	to: function ( params ) {
        this.init(params);

        if ( this.isSmoothScrollSupported ) {
            window.scrollTo(this.options)
        } else {
            window.scrollTo(this.options.left, this.options.top);
        }
    }

};
