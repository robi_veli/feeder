import Tabby from 'tabbyjs';

export default {

    options: {
        el: '[data-tabs]'
    },

    init: function () {
        this.setupTabby();
    },

    setupTabby: function () {
        const tabModule = new Tabby(this.options.el);
    }
};
