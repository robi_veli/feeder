import scroll from './scroll';

export default {

    options: {
        button: '.buttonPoint--typeDown',
        scrollToContainer: '.blockSection--typeArticle'
    },

    init: function () {
        this.setupEvents();
    },

    setupEvents: function () {

        document.querySelector(`${this.options.button}`).addEventListener('click', () => {
            scroll.to({left: 0, top: document.querySelector(`${this.options.scrollToContainer}`).offsetTop});
        });
    }

};
